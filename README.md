#说明
本项目fork自`https://github.com/ZZMarquis/gmhelper`
和`https://github.com/FISCO-BCOS/paillier-lib`
编译报错时需配置gradle.properties文件

```bash
NEXUS_USERNAME=_your_name_
NEXUS_PASSWORD=_your_passowrd_
signing.keyId=_your_own_key_id_
signing.password=_keyid_passowrd_
signing.secretKeyRingFile=/path/to/the/key.pgp
```